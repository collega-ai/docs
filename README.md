# Documents
Collega AI supports the following document formats:

## Files
When uploading a file, ensure that it meets the following requirements:

- Not corrupted
- Not password protected
- Contains textual data, not images, graphics, etc.
- Weighs no more than 5 MB


## Google doc
When uploading a link to a Google doc, ensure that:

- The document is available for public access
- The document is in the google doc format

*collega.ai temporarily does not support google sheets and other formats*

### Common issues
If the document you want to attach was not created using google doc but was imported there, there might be compatibility issues. In such a case, it's better to use the file upload feature and upload the original document.

**How to check the google doc format:**

Example of a "correct" google doc:

![IMAGE_DESCRIPTION](images/true_g-doc.png)

Example of an "incorrect" google doc:

![IMAGE_DESCRIPTION](images/false_g-doc.png)

Note the hint about the file format.

## Youtube
When uploading a link to Youtube, ensure that:

- The video is available for public access
- The video has subtitles in one of the following languages: English, Spanish, French.
